package com.example.android.notesapp.viewmodal;

import android.content.Context;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.MutableLiveData;

import com.example.android.notesapp.databinding.ActivityAddNotesBinding;
import com.example.android.notesapp.db.NoteEntity;
import com.example.android.notesapp.modal.Note;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AddNoteViewModel extends BaseObservable {
    public MutableLiveData<Boolean> closeActivity;
    private Context context;
    private Note note;
    public AddNoteViewModel(Context context){
        note=new Note();
        this.context = context;
    }
    @Bindable
    public String getTitle(){
        return note.getTitle();
    }
    public void setTitle(String title){
        note.setTitle(title);
    }
    @Bindable
    public String getDescription(){
        return note.getDescription();
    }
    public void setDescription(String description){
        note.setDescription(description);
    }
    public void addNote(){
        String title = note.getTitle();
        String description = note.getDescription();
        if(title != null && description != null && title.trim().length() != 0 && description.trim().length() != 0){
            Date date = new Date();
            String pattern = "MM/dd/yyyy HH:mm:ss";
            DateFormat df = new SimpleDateFormat(pattern);
            String currentDate = df.format(date);
            Log.i("currentDate",""+currentDate);
            Log.i("date",""+date);
            note.setDate(currentDate);
            //note.setDate();
            DataHandler db = new DataHandler(context);
            db.insert(Converter.noteToNoteEntity(note));
            closeActivity.setValue(true);
        }
        else{
            Toast.makeText(context,"Empty Field!",Toast.LENGTH_SHORT).show();
        }
    }
    public MutableLiveData<Boolean> getCloseActivity() {
        if (closeActivity == null) {
            closeActivity = new MutableLiveData<Boolean>();
        }
        return closeActivity;
    }
}
