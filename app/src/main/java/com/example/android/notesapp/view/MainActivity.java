package com.example.android.notesapp.view;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.android.notesapp.databinding.ActivityMainBinding;
import com.example.android.notesapp.db.NoteEntity;
import com.example.android.notesapp.viewmodal.Converter;
import com.example.android.notesapp.viewmodal.DataHandler;
//import com.example.android.notesapp.viewmodal.DatabaseHandler;
import com.example.android.notesapp.viewmodal.MainViewModel;
import com.example.android.notesapp.viewmodal.MyNotesAdapter;
import com.example.android.notesapp.R;
import com.example.android.notesapp.modal.Note;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private MyNotesAdapter adapter;

    //private DatabaseHandler db;
    private   RecyclerView recyclerView;

    //
    private MainViewModel mainViewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);
        //
        mainViewModel = new MainViewModel(this);
        ActivityMainBinding main = DataBindingUtil.setContentView(this,R.layout.activity_main);
        main.setMainViewModel(mainViewModel);
        main.executePendingBindings();

        Context context = this;




        mainViewModel.getList().observe(this, new Observer<List<Note>>() {
            @Override
            public void onChanged(List<Note> notes) {
                for(Note x:notes)Log.i("mutableList",""+x.getTitle());
                if(notes != null){
                    Collections.sort(notes,(Note a,Note b)->b.getDate().compareTo(a.getDate()));

                    recyclerView = findViewById(R.id.recyclerView);
                    adapter = new MyNotesAdapter(notes,context);
                    recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
                    recyclerView.setAdapter(adapter);
                }
                else{
                    Log.e("null list","000");
                }
            }
        });




    }
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == R.id.delete_all){
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.setTitle(R.string.delete_all_notes_dialog_heading);
            alertDialog.setMessage(R.string.delete_all_notes_dialog);
            alertDialog.setPositiveButton(android.R.string.yes, (DialogInterface dialog, int which)-> {
                DataHandler dh = new DataHandler(this);
                dh.deleteAll();
                recreate();
                    }
            );
            // A null listener allows the button to dismiss the dialog and take no further action.
            alertDialog.setNegativeButton(android.R.string.no, null);
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.show();
        }
        else if(item.getItemId() == R.id.sort_by_oldest){
            List<Note> notes = mainViewModel.setAdapterOldToNew();
            adapter = new MyNotesAdapter(notes,getApplicationContext());
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setAdapter(adapter);
        }
        else if(item.getItemId() == R.id.sort_by_newest){
            List<Note> notes = mainViewModel.setAdapterNewToOld();
            adapter = new MyNotesAdapter(notes,getApplicationContext());
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerView.setAdapter(adapter);
        }
        return super.onOptionsItemSelected(item);
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) item.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                adapter.notifyDataSetChanged();
                return false;
            }
        });
        return super.onCreateOptionsMenu(menu);
    }
    @Override
    protected void onResume() {
            super.onResume();
            mainViewModel.getList();
    }
}