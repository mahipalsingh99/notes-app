package com.example.android.notesapp.db;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface NoteDao {
    @Insert
    void insert(NoteEntity... notes);
    @Delete
    void delete(NoteEntity note);
    @Update
    void update(NoteEntity note);
    @Query("SELECT * FROM noteentity")
    List<NoteEntity> getAllNotes();
    @Query("DELETE FROM noteentity")
    void deleteALlNotes();
//    @Query("DELETE FROM note WHERE uid = :uid")
//    void deleteByUserId(long uid);

}
