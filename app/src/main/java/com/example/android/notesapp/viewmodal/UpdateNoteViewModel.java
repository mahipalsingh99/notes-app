package com.example.android.notesapp.viewmodal;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.example.android.notesapp.modal.Note;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UpdateNoteViewModel extends BaseObservable {
    public MutableLiveData<Boolean> closeActivity;
    private Note note;
    //private DatabaseHandler db;
    private Context context;
    public UpdateNoteViewModel(Note note, Context context){
        this.context = context;
        //db = new DatabaseHandler(context);
        this.note = note;
    }
    @Bindable
    public String getTitle(){
        return note.getTitle();
    }
    public void setTitle(String title){
        note.setTitle(title);
    }
    @Bindable
    public String getDescription(){
        return note.getDescription();
    }
    public void setDescription(String description){
        note.setDescription(description);
    }

    public void updateNote(){
        String title = note.getTitle();
        String description = note.getDescription();
        if(title != null && description != null && title.length() != 0 && description.length() != 0) {
            Date date = new Date();
            String pattern = "MM/dd/yyyy HH:mm:ss";
            DateFormat df = new SimpleDateFormat(pattern);
            String currentDate = df.format(date);
            Log.i("currentDate",""+currentDate);
            Log.i("date",""+date);
            note.setDate(currentDate);

            DataHandler dh = new DataHandler(context);
            dh.update(Converter.noteToNoteEntity(note));
            closeActivity.setValue(true);
        }
        else{
            Toast.makeText(context,"Empty Field!",Toast.LENGTH_SHORT).show();
        }
    }
    public MutableLiveData<Boolean> getCloseActivity() {
        if (closeActivity == null) {
            closeActivity = new MutableLiveData<Boolean>();
        }
        return closeActivity;
    }

}
