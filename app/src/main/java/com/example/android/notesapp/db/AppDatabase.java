package com.example.android.notesapp.db;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.android.notesapp.modal.Note;

@Database(entities = NoteEntity.class,version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract NoteDao noteDao();
    private static AppDatabase INSTANCE;
    public static AppDatabase getInstance(Context context){
        if(INSTANCE == null){
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),AppDatabase.class,"NOTE_DB")
                    .allowMainThreadQueries().build();
        }
        return INSTANCE;
    }
}
