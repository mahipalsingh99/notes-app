package com.example.android.notesapp.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.android.notesapp.databinding.ActivityUpdateNoteBinding;
//import com.example.android.notesapp.viewmodal.DatabaseHandler;
import com.example.android.notesapp.R;
import com.example.android.notesapp.modal.Note;
import com.example.android.notesapp.viewmodal.UpdateNoteViewModel;

public class UpdateNoteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_update_note);
        Note note = (Note)getIntent().getSerializableExtra("note");
        UpdateNoteViewModel updateNoteViewModel = new UpdateNoteViewModel(note,this);
        ActivityUpdateNoteBinding activityUpdateNoteBinding = DataBindingUtil.setContentView(this,R.layout.activity_update_note);
        activityUpdateNoteBinding.setUpdateNoteViewModel(updateNoteViewModel);
        activityUpdateNoteBinding.executePendingBindings();
        //close the activity after updating the value
        updateNoteViewModel.getCloseActivity().observe(this,(Boolean closeActivity)->{
            if(closeActivity){
                updateNoteViewModel.getCloseActivity().setValue(false);
                finish();
            }
        });
        //empty string validation
        activityUpdateNoteBinding.titleUpdate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0){
                    activityUpdateNoteBinding.titleUpdate.setError("Title cannot be Empty !");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //request focus on edit text for description
        activityUpdateNoteBinding.descriptionUpdate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0){
                    activityUpdateNoteBinding.descriptionUpdate.setError("Description cannot be Empty !");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }
}