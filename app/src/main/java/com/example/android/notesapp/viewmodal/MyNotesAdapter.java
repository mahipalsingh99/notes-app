package com.example.android.notesapp.viewmodal;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android.notesapp.R;
import com.example.android.notesapp.modal.Note;
import com.example.android.notesapp.view.UpdateNoteActivity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;

public class MyNotesAdapter extends RecyclerView.Adapter<MyNotesAdapter.ViewHolder> implements Filterable {
    private  List<Note> listNote;
    private List<Note> listNoteAll;
    private  Context context;
    public MyNotesAdapter(List<Note> listNote,Context context) {
        this.listNoteAll = new ArrayList<>(listNote);
        this.listNote = listNote;
        this.context = context;
    }
    //search filter

    @Override
    public Filter getFilter() {
        return filter;
    }
    Filter filter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Note> filteredList = new ArrayList<>();
            if(constraint.toString().trim().isEmpty()){
                filteredList.addAll(listNoteAll);
            }
            else{
                for(Note currentNote:listNoteAll){
                    if(currentNote.getTitle().toLowerCase().contains(constraint.toString().toLowerCase()) ||
                            currentNote.getDescription().toLowerCase().contains(constraint.toString().toLowerCase())){
                        filteredList.add(currentNote);
                    }
                }
            }
            FilterResults filterResults = new FilterResults();
            filterResults.values = filteredList;
            return filterResults;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            listNote.clear();
            listNote.addAll((Collection<? extends Note>)results.values);
            notifyDataSetChanged();
        }
    };


    //view holder class

    public  class ViewHolder extends RecyclerView.ViewHolder implements Serializable {
        public TextView titleTextView;
        public TextView descriptionTextView;
        public RelativeLayout relativeLayout;
        public ImageButton deleteButton;

        public ViewHolder(View itemView) {
            super(itemView);

            titleTextView =  itemView.findViewById(R.id.titleTextView);
            descriptionTextView =  itemView.findViewById(R.id.descriptionTextView);
            relativeLayout = itemView.findViewById(R.id.relativeLayout);
            deleteButton = itemView.findViewById(R.id.select_delete_button);

            itemView.setOnClickListener(v -> {
                //for adding a note
                int position = getAdapterPosition();
                Intent intent = new Intent(context, UpdateNoteActivity.class);
                intent.putExtra("note", (Serializable) listNote.get(position));
                context.startActivity(intent);
            });
        }
    }
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem= layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //final Note note = listNote.get(position);
        holder.titleTextView.setText(listNote.get(position).getTitle());
        holder.descriptionTextView.setText(listNote.get(position).getDescription());

        holder.deleteButton.setOnClickListener((View v)->{

            //alert for delete button

            AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
            alertDialog.setTitle("Delete this Note!");
            alertDialog.setMessage("Are you sure you want to delete this Note?");
            alertDialog.setPositiveButton(android.R.string.yes, (DialogInterface dialog, int which)-> {
                        int pos = holder.getAdapterPosition();
                        DataHandler dh = new DataHandler(context);
                        dh.delete(Converter.noteToNoteEntity(listNote.get(pos)));
                        listNote.remove(pos);
                        listNoteAll.remove(pos);
                        notifyItemRemoved(pos);
                        }
                    );

            // A null listener allows the button to dismiss the dialog and take no further action.
            alertDialog.setNegativeButton(android.R.string.no, null);
            alertDialog.setIcon(android.R.drawable.ic_dialog_alert);
            alertDialog.show();
        });
    }
    @Override
    public int getItemCount() {
        if(listNote == null)return 0;
        Log.e("size",listNote.size()+"");
        return listNote.size();
    }
}  
