package com.example.android.notesapp.viewmodal;

import android.content.Context;
import android.content.Intent;
import android.view.View;

import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.android.notesapp.R;
import com.example.android.notesapp.db.NoteEntity;
import com.example.android.notesapp.modal.Note;
import com.example.android.notesapp.view.AddNotesActivity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MainViewModel {
    private MutableLiveData<List<Note>> list;
    private List<Note> notes= new ArrayList<>();

    private Context context;
    public MainViewModel(Context context){
        this.context = context;
        list = new MutableLiveData<>();
    }
    public void addNewNote(){
        Intent intent = new Intent(context, AddNotesActivity.class);
        context.startActivity(intent);
    }

    public MutableLiveData<List<Note>> getList() {
        DataHandler dh = new DataHandler(context);
        List<Note> temp = new ArrayList<>();
        for(NoteEntity noteEntity : dh.getAllNotes()){
            temp.add(Converter.noteEntityToNote(noteEntity));
        }
        list.setValue(temp);
        return list;
    }
    public List<Note> setAdapterNewToOld(){
        DataHandler dataHandler = new DataHandler(context);
        notes.clear();
        for(NoteEntity noteEntity:dataHandler.getAllNotes()){
            notes.add(Converter.noteEntityToNote(noteEntity));
        }
        Collections.sort(notes,(Note a,Note b)->b.getDate().compareTo(a.getDate()));
       return notes;
    }
    public List<Note> setAdapterOldToNew(){
        DataHandler dataHandler = new DataHandler(context);
        notes.clear();
        for(NoteEntity noteEntity:dataHandler.getAllNotes()){
            notes.add(Converter.noteEntityToNote(noteEntity));
        }
        Collections.sort(notes,(Note a, Note b)->a.getDate().compareTo(b.getDate()));
        return notes;

    }
}
