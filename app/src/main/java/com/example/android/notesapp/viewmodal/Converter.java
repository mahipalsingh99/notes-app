package com.example.android.notesapp.viewmodal;

import com.example.android.notesapp.db.NoteEntity;
import com.example.android.notesapp.modal.Note;

public interface Converter {
    static NoteEntity noteToNoteEntity(Note note){
        NoteEntity noteEntity = new NoteEntity();
        noteEntity.setUid(note.getId());
        noteEntity.setTitle(note.getTitle());
        noteEntity.setDescription(note.getDescription());
        noteEntity.setDate(note.getDate());
        return noteEntity;
    }
    static Note noteEntityToNote(NoteEntity noteEntity){
        Note note = new Note();
        note.setId(noteEntity.getUid());
        note.setTitle(noteEntity.getTitle());
        note.setDescription(noteEntity.getDescription());
        note.setDate(noteEntity.getDate());
        return note;
    }
}
