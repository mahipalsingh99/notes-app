package com.example.android.notesapp.viewmodal;

import android.content.Context;

import com.example.android.notesapp.db.AppDatabase;
import com.example.android.notesapp.db.NoteEntity;

import java.util.List;

public class DataHandler {
    Context context;
    public DataHandler(Context context){
        this.context = context;
    }
    //insert Note
    public void insert(NoteEntity note){
        AppDatabase.getInstance(context).noteDao().insert(note);
    }
    //delete Note
    public void delete(NoteEntity note){
        AppDatabase.getInstance(context).noteDao().delete(note);
    }
    //update Note
    public void update(NoteEntity note){
        AppDatabase.getInstance(context).noteDao().update(note);
    }
    //delete All Notes
    public void deleteAll(){
        AppDatabase.getInstance(context).noteDao().deleteALlNotes();
    }
    //get All Notes
    public List<NoteEntity> getAllNotes(){
       return AppDatabase.getInstance(context).noteDao().getAllNotes();
    }
}
