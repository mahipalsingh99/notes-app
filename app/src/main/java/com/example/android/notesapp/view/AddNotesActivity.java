package com.example.android.notesapp.view;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;


import com.example.android.notesapp.databinding.ActivityAddNotesBinding;
import com.example.android.notesapp.viewmodal.AddNoteViewModel;
import com.example.android.notesapp.R;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

public class AddNotesActivity extends AppCompatActivity {

    private AddNoteViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = new AddNoteViewModel(this);
        //setContentView(R.layout.activity_all_notes);
        ActivityAddNotesBinding activity = DataBindingUtil.setContentView(this,R.layout.activity_add_notes);
        activity.setAddNoteViewModel(viewModel);
        activity.executePendingBindings();

        // Create the observer which updates the UI.
        final Observer<Boolean> nameObserver = new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable final Boolean closeActivity) {
                // Update the UI, in this case, a TextView.
                if(closeActivity){
                    viewModel.getCloseActivity().setValue(false);
                    finish();
                }

            }
        };

        viewModel.getCloseActivity().observe(this, nameObserver);

        //empty string validation
        activity.title.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0){
                    activity.title.setError("Title cannot be Empty !");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        //request focus on edit text for description
        activity.description.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if(s.length()==0){
                    activity.description.setError("Description cannot be Empty !");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

}